console.log('before');
const user = getUser(1);
console.log(user);
console.log('after');

function getUser(id){
  setTimeout(() => {
    console.log('msg after 2 secs');
    return id;
  }, 2000);
}
