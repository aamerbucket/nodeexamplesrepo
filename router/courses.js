const express = require('express');
const router = express.Router();

  // set an array
  const courses = [
    { id: 1, name: 'course 1' },
    { id: 2, name: 'course 2' },
    { id: 3, name: 'course 3' }
  ];

// -------------------------------------------------------
// Read all courses
  router.get('/', function(req, res){
    res.send(courses);
   });

// ---------------------------- Read ---------------------------
// Read by id
  router.get('/:id', function(req, res){
    // check course exists or not
    const course = courses.find(c => c.id === parseInt(req.params.id));
    // here course is not found then print error
      if(!course){
        return res.status(404).send("Course not found ");
      }
      // if found return all courses
      res.send(course);
  });

// ----------------------- Create or add --------------------------------
// create or add course
    router.post('/', function(req, res){
        // first check, does the course has proper input
        if(!req.body.name || req.body.name.length < 3){
          // 400 bad request
          return res.status(400).send("name should not be empty or less than 3 char");
          }
        // else add the course
          //pass an object
          const course = {
            id: courses.length + 1,
            name: req.body.name  // passing a json object in the body of req object, so initialize router.use(express.json())
          }
        courses.push(course);
        // send to client
        res.send(courses);
    });

  // ------------------------- Update ------------------------------
    router.put('/:id', function (req, res){
      // course is exist or not
      const course = courses.find(c => c.id === parseInt(req.params.id));
      if(!course){
        return res.status(404).send("Course is not found ");
      }
      // if course is exist, validate
      if(!req.body.name || req.body.name.length < 3){
        // 400 bad request
        return res.status(400).send("name should not be empty or less than 3 char");
        }
      // if above both conditions done then update the course
      course.name = req.body.name;
      res.send(courses);
    });

    // ------------------------- Delete ------------------------------
    router.delete('/:id', function (req, res){
      // course is exist or not
      const course = courses.find(c => c.id === parseInt(req.params.id));
      if(!course){
        return res.status(404).send("Course is not found ");
      }
      // if found delete it, using indexOf and splice objects
      const index = courses.indexOf(course);
      courses.splice(index, 1); // it removes one object
      // send res
      res.send(courses);
    });


module.exports = router;
