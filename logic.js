var options = {
  type: 'line',
  data: {
    labels: ["2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"],
    datasets: [
	    {
	      label: 'Soil Moisture',
	      data: [25, 43, 32, 28, 40, 22, 24, 21, 15, 17, 23, 34],
      	borderWidth: 1
    	}
		]
  },
  options: {
  	scales: {
    	yAxes: [{
        ticks: {
					reverse: false
        }
      }]
    }
  }
}

var ctx = document.getElementById('chartJSContainer').getContext('2d');
new Chart(ctx, options);
