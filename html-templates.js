const express = require('express');
const app = express();
app.use(express.json());

// set pug template
app.set('view engine', 'pug');

// pass where you have created or stored the template (optional)
app.set('views', './views');

app.listen('3000', () => {
  console.log("Server started on ...");
});


app.get('/', function(req, res){
   res.render('index', { title: 'My Express App', message: 'Hello World', msgTwo: "2nd msg" });
 });
