console.log('before');
getUser(1, function (user) {
  console.log('user', user);
});
console.log('after');

function getUser(id, callback){
  setTimeout(() => {
    console.log('msg after 2 secs');
    callback({ id: id, gitUsername: 'aamer'});
  }, 2000);
}
