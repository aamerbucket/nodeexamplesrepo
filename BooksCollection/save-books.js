// load db module
const mongoose = require('mongoose');

// establish the connection
mongoose.connect('mongodb://localhost/playground')
    .then( () => console.log('Connected to the database...') )
    .catch ( err => console.error('Counld not connect to db...', err.message));

// create a schema
const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    date: { type: Date, default: Date.now },
    isPublished: Boolean
});

// Compile schema into the Model
const Course = mongoose.model('courseTable', courseSchema);

// save the course into the database
async function createCourse(){
    try{
        const course = new Course({
            name: 'CCNA',
            author: 'Faiz',
            tags: ['CCNA', 'Networking'],
            isPublished: true
        });
        const courseId = await course.save();
        console.log('You have saved the document with the ID: ', courseId);
    }
    catch (err){
        console.log('Error ', err.message);
    }
}

createCourse();


