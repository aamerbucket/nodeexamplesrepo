const debugGeneralMsgs = require('debug')('app:debugGeneralMsgs');
const debugDBMsgs = require('debug')('app:database');
const express = require('express');
const app = express();
app.use(express.json());

// suppose here is db code
debugDBMsgs('DATABASE is connected');


app.listen('3000', () => {
  debugGeneralMsgs("Server started on ...");
});
