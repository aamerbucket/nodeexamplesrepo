const express = require('express');
const app = express();
app.use(express.json());

// set an array
const courses = [
  { id: 1, name: 'course 1' },
  { id: 2, name: 'course 2' },
  { id: 3, name: 'course 3' }
];

// get all courses
app.get('/list/courses', function(req, res){
   res.send(courses);
 });

// get specific course using id  -- get is used to fetch the data
app.get('/list/courses/:id', function(req, res){
    // here we are comparing courses array single value with single param value from url (user)
    // there is a function called find
    // check courses array using find
    const course = courses.find(c => c.id === parseInt(req.params.id));
    // here course is not found then print error
    if(!course){
      res.status(404).send("Course not found ");
    }
   res.send(course);
 });
 app.post('/list/course', function(req, res){
   //pass an object
   const course = {
     id: courses.length + 1,
     name: req.body.name  // passing a json object in the body of req object, so initialize app.use(express.json())
   }
   // push this object to array list
   courses.push(course);
   // send the update code to the client
   res.send(course);
 });

app.listen('3000', () => {
  console.log("Server started on ...");
});
