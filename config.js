const config = require('config');
const express = require('express');
const app = express();
app.use(express.json());

app.listen('3000', () => {
  console.log("Server started on ...");
});

console.log("get process properties" + config.get('name'));
console.log("Mail name" + config.get('mail.host'));
