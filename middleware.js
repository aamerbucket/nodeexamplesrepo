const express = require('express');
const app = express();
app.use(express.json());

//------------------------------ start the server ----------------------
app.listen('3000', () => {
  console.log("Server started on ...");
});

// add middel ware function
// create a function in other js file
// load the function
const middleWare = require('./middleware-function');
app.use(middleWare);

app.get('/home', function(req, res){
  res.send("Home")
});
