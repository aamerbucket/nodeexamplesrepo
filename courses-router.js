const courses = require('./router/courses');
const express = require('express');
const app = express();
app.use(express.json());

// tell express to use courses if url starts with /list/courses
app.use('/list/courses', courses);

app.listen('3000', () => {
  console.log("Server started on ...");
});
