const express = require('express');
const app = express();
const router = require('./router/crud-route')
app.use(express.json());
app.use('/list/courses', router);

// start the server
app.listen('3000', () => {
  console.log("Server started on ...");
});
